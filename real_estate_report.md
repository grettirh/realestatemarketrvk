
# Modelling Real Estate Prices in Reykjavík Area with XGBoost

## Abstract
In this report we're going to create a XGBoost model in R on features provided by Registry Iceland to predict the price of a property. Our objective is to try to maximize predictive power. In doing that we'll do some data cleaning and feature engineering. For the data cleaning part we'll mostly rely on the R `tidyverse` package system. For the feature engineering we'll use our domain knowledge of the data to create new features and also put an upper bound on some of the features to limit the state space. We'll also try some automatic feature engineering in the modelling part. For the modelling part we'll rely on the R `mlr` (machine learning in R) package which is a framework for doing machine learning. We're going to use a tuned XGBoost model for our training, the main reason being it's performance and speed. It has it's drawbacks though, one being it doesn't extrapolate well. So we can't really use it make future predictions. To wrap it all up we'll do model performance checks and some visualization of predictions.

## Data Cleaning
In this part we're going to get the dataset from Registry Iceland and clean it up. We're going to remove redundant data and features and make sure they're all of the correct data type.

### R Code
```r
# load the libraries
library(dplyr)
library(lubridate)
library(magrittr)
library(summarytools)

# the path to the dataset
url_path = paste0(
  "https://www.skra.is/library/Samnyttar-skrar-/Fyrirtaeki-stofnanir/",
  "Fasteignamat-2019/gagnasafn_ib_2018.csv")
# fetch the dataset
df_raw = read.csv2(url_path)
# filter on vars used and capital area and remove small teg_eign categories
# and stig10 < 10 and one outlier m2_price
df = df_raw %>%
  group_by(teg_eign) %>% filter(n()>100) %>% ungroup() %>%
  filter(matssvaedi<999,svfn<2000,stig10==10)
# select the variables we're using and rename them so they're more
# descriptive
df %<>% select(
    id=rfastnum,price_adj=nuvirdi,purchase_date=kdagur,
    area=matssvaedi,subarea=undirmatssvaedi,property_type=teg_eign,
    appartment_type=ibteg,construction_year=byggar,property_m2=birtm2,
    appartment_m2=ibm2,netto_m2=ntm2,area_under_roof_m2=rism2,
    garage_m2=bilskurm2,parking_house_m2=bilgm2,balcony_m2=svalm2,
    main_floor_m2=ib1m2,basement_m2=ib2m2,under_roof_m2=ib3m2,
    floors_no=fjhaed,rooms_no=fjherb,livingrooms_no=fjstof,toilets_no=fjklos,
    kitchens_no=fjeld,storages_no=fjgeym,bathtubs_no=fjbkar,showers_no=fjsturt,
    floor_nr=haednr,top_floor_nr=efstah,appartments_in_building_no=fjibmhl,
    elevators_no=lyfta,circumference=ummal,rating_index=studull,
    appartments_on_floor_no=fjibhaed)
# set kdagur to date
df %<>% mutate(purchase_date = dmy(purchase_date))
# set byggar to date going with january 1.
df %<>% mutate(construction_year = dmy(paste0("0101",construction_year)))
# set some m2 vars to numeric.
m2Cols = c("property_m2", "area_under_roof_m2","area_under_roof_m2",
    "garage_m2", "balcony_m2")
df %<>% mutate_at(m2Cols, ~as.numeric(as.character(gsub(",",".",.))))
# set fastnum to character
df %<>% mutate(id=as.character(id))
# cast negative values to NA
df %<>% mutate_if(is.numeric,~ifelse(.<0,NA,.))
# recode area
value_zone = tribble(
  ~code, ~zone,
  "11" , "Vesturbær: Vestan Bræðraborgarstígs",
  "20" , "Miðbær: Frá Tjörn að Snorrabraut",
  "25" , "Miðbær: Frá Bræðraborgarstíg að Tjörn",
  "31" , "Miðbær: Suður-Þingholt",
  "70" , "Melar: Að sjó",
  "72" , "Hagar/Melar: Vestan Hofsvallagötu",
  "75" , "Skerjafjörður",
  "80" , "Hlíðar",
  "85" , "Kringlan",
  "90" , "Holt/Tún",
  "91" , "Háaleiti/Skeifa",
  "100", "Laugarneshverfi/Vogar",
  "110", "Bryggjuhverfi",
  "120", "Grafarvogur: Hamrar, Foldir, Hús",
  "130", "Grafarvogur: Rimar, Engi, Víkur, Borgir",
  "140", "Grafarvogur: Staðir",
  "150", "Seljahverfi",
  "160", "Hólar, Berg",
  "161", "Fell",
  "170", "Neðra-Breiðholt",
  "180", "Grafarholt",
  "181", "Úlfarsárdalur",
  "200", "Árbær",
  "210", "Ártúnsholt/Höfðar",
  "220", "Selás",
  "270", "Norðlingaholt",
  "280", "Fossvogur",
  "281", "Réttarholt",
  "282", "Blesugróf",
  "283", "Hvassaleiti",
  "284", "Bústaðahverfi",
  "290", "Kjalarnes",
  "300", "Kópavogur: Vesturbær",
  "320", "Kópavogur: Austurbær",
  "330", "Kópavogur: Hjallar, Smárar",
  "340", "Kópavogur: Lindir, Salir",
  "350", "Kópavogur: Hvörf, Þing",
  "351", "Kópavogur: Kórar",
  "400", "Seltjarnarnes",
  "500", "Garðabær",
  "510", "Garðabær: Sjáland",
  "511", "Garðabær: Akrahverfi",
  "520", "Garðabær: Ásar",
  "530", "Garðabær: Vestan Hraunholtsbrautar",
  "540", "Garðabær: Arnarnes",
  "550", "Garðabær: Urriðaholt",
  "560", "Garðabær: Flatir",
  "590", "Garðabær: Utan þéttbýlis",
  "600", "Hafnarfjörður",
  "620", "Hafnarfjörður: Hvaleyrarholt",
  "630", "Hafnarfjörður: Vellir",
  "640", "Hafnarfjörður: Ásland",
  "650", "Hafnarfjörður: Setberg",
  "660", "Hafnarfjörður: Flensborg",
  "670", "Hafnarfjörður: Sunnan byggðar",
  "680", "Hafnarfjörður: Börð",
  "700", "Garðabær: Álftanes",
  "800", "Mosfellsbær",
  "810", "Mosfellsbær: Teigar, Krikar",
  "820", "Mosfellsbær: Leirvogstunga",
  "830", "Mosfellsbær: Mosfellsdalur",
  "840", "Mosfellsbær: Höfðar, Hlíðar",
  "850", "Mosfellsbær: Helgafell",
  "890", "Mosfellsbær: Utan þéttbýlis")
df %<>% mutate(area =
  factor(area,levels=value_zone$code,labels=value_zone$zone))
# set subarea as factor
df %<>% mutate(subarea = factor(subarea,levels=sort(unique(subarea))))
# recode appartment_type
df %<>% mutate_at("appartment_type", ~ifelse(.==11,"sérbýli","fjölbýli"))
df %<>% mutate(appartment_type = as.factor(appartment_type))
# set the price_adj to correct format
df %<>% mutate(price_adj = price_adj*1000)
# drop empty factor levels
df %<>% droplevels()
# drop variables with missing values
df %<>% filter(complete.cases(.))

# Create summary report
options(scipen=1000000)
descr(df, stats=c("mean", "sd", "min", "q1", "med", "q3", "max"),
  transpose=TRUE, style="rmarkdown") %>% knitr::kable(digits=1)
```

## Feature Engineering

Now we're going to engineer the data so it will better fit into our XGBoost model.

We're going to start by capping the possible values for the numerical variables. The reason is that we don't want the learner to make inference about events that are highly unlikely. We'll make a simple rule. We'll cap the bins at the 0.0001 and 0.9999 quantiles. That is only 0.02% cap per feature. We'll make a note on how many lines are changed with this method.

```r
# cap the numerics to a 0.0001 and 0.9999 quantile range
df_old = df
df %<>% mutate_if(is.numeric,
  ~pmax(pmin(.,quantile(.,0.9999)),quantile(.,0.0001)))
df %>% anti_join(df_old) %>% tally() #N=64
rm(df_old)
```

With this we only affected 64 measurements and got rid of the most extreme values in the dataset.

We're also going to make all the square meter variables as a fraction of the total property area. This is nice since get all the variables on the same scale and we think that those variable only matter in relation to the total area of the property.

```r
# set all m2 as percentage of property_m2
m2Cols = grep("m2", colnames(df), value=TRUE) %>% setdiff("property_m2")
df %<>% mutate_at(m2Cols, ~./property_m2)
```

Next we will create a total room number variable. So we'll count every room equally and get rid of all the others. This we reduce our feature space which makes it easier for training.

```r
# Add together all rooms for a single total_rooms_no variable
df %<>% mutate(total_rooms_no =
    rooms_no+livingrooms_no+toilets_no+kitchens_no+storages_no) %>%
  select(-c("livingrooms_no","toilets_no","kitchens_no","storages_no"))
```

Create a new variable telling us the price of last selling price of the property along with date.

```r
# Create a new variable telling us the price and date of last sale
df %>% group_by(id) %<>%
  mutate(last_sold_price = lag(price_adj, 1, order_by=purchase_date),
    last_sold_date = lag(purchase_date, 1, order_by=purchase_date))
```

Some small feature engineering
```r
# Add bathtubs and showers and let shower be an indicator variable
df %<>% mutate(
  showers_bathtubs_no = showers_no+bathtubs_no,
  has_shower = 1*(showers_no>0)) %>% select(-showers_no, -bathtubs_no)
# Make elevator an indicator variable
df %<>% mutate(has_elevator = 1*(elevators_no>0)) %>% select(-elevators_no)
# Make a new variable that tells us the age of the property when it was bought
df %<>% mutate(
  property_age_when_sold = year(purchase_date)-year(construction_year))

# Save the dataset for later usage
save(df, file="HGG/df.RData")
```

That's that. We'll handle the factor date features engineering in the model building stage. Let's plot our data to get an idea of what we're dealing with.

![Adjusted price plot](img/real_estate_report_price_adj.png)
![Area plot](img/real_estate_report_area.png)
![Subarea plot](img/real_estate_report_subarea.png)
![Property types plot](img/real_estate_report_property_types.png)
![Square meter plot](img/real_estate_report_m2.png)
![Counting variables plot](img/real_estate_report_count.png)
![Date variables plot](img/real_estate_report_date.png)
![Other variables plot](img/real_estate_report_other.png)

## Modelling with XGBoost

Now we get to the fun part. Our features are nice and clean after the previous steps and we can begin to train our model.

### Preprocessing dates and factors

We will be changing all our date variables to numeric since our modelling framework can't handle data data. We're going to keep all the dates stored in a key dataframe so we can get the original dates back when evaulating the model.

We will preprocess our factors in the same way, casting them all to numeric. We can do that since there is some natural ordering of the factor levels, numbers close together for the `area` variable tend to be geographically close. We're not sure about the `subarea` variable but we're allowing it since it simplifies our learning process. The casting of factors to numbers will be done in the learning stage so our dataset will keep the initial factor levels.

### Defining The Learner and Training Task

We're going to use a XGBoost regression tree model. Our objective will use a Gamma link function since our target variable price has heavy tails and is skewed. We're going for a histogram tree method since it's faster.

For our training task we will use the data preprocessed in previous sections with `price_adj` (price adjusted for inflation) as the target variable. We will exclude the `id` variable from the task but use all others.

We're defing our learner and training task in R mlr framework which allows for some high level operations.

### R Code
```r
################################################################################
#---------------------------PREPROCESS, LEARNER, TASK--------------------------#
################################################################################

# Load data, libs and apply settings
library(dplyr)
library(mlr)
library(mlrCPO)
library(lubridate)
library(magrittr)
# Load the df
load("HGG/df.RData")
# internal stuff for mlr
configureMlr(on.par.without.desc="warn")

# Preprocessing
#date keys
dateKeys = df %>% select_if(is.Date)
#cast df date to numerics
df = df %<>% mutate_if(is.Date, as.numeric)
#convert all factors to numeric cpo
cpos = cpoAsNumeric()

# Define Learner and Task
#define the learner
learner = makeLearner("regr.xgboost")
#make the learner run with reg:gamma, gbtree and hist.
learner = learner %>% setHyperPars(par.vals=
    list(objective="reg:gamma",booster="gbtree",tree_method ="hist"))
#apply the cpo to the learner
learner_cpo = cpos %>>% learner
# Make a regression task with price_adj as target and remove id
task = makeRegrTask(data=within(df, rm(id)), target="price_adj")
```

## Model Tuning

We're going to tune the hyperparameters of th XGBoost model so it will find a viable set of hyperparameters to train the learner.

### Tuning Parameters

The parameters we're going to tune are summed up in the following table:

|Hyperparameter    |Explanation                                           |
|:-----------------|:-----------------------------------------------------|
|`nrounds`         |The number of rounds for boosting                     |
|`max_depth`       |Maximum depth of a tree                               |
|`eta`             |Step size shrinkage used in update                    |
|`colsample_bytree`|subsample ratio of columns when constructing each tree|
|`subsample`       |Subsample ratio of the training instances             |

We suspect that the model will be complex so we're going to allow `nrounds` and `max_depth` to be large and we're using `eta`, `colsample_bytree` and `subsample` to prevent overfitting. The tuning parameters are as follows

|Hyperparameter    |  Lower bound|  Upper Bound|
|:-----------------|------------:|------------:|
|`nrounds`         |          500|         2500|
|`max_depth`       |            3|           15|
|`eta`             |        0.001|          0.2|
|`colsample_bytree`|          0.1|            1|
|`subsample`       |          0.5|            1|

### Hyperparameter Search Strategy

In the search of the best set of hyperparameters we're going for an MBO (Model Based Optimization) strategy. What this strategy does is it fit's a surrogate model over the space of hyper-parameters and searches for promising points that maximize our performance metric. Our mbo uses Kriging to search for those points. We do 100 iterations of this search which will hopefully lead to a set a hyperparameters that perform well in cross validation.

### Performance Metrics and Validation Strategy

When tuning our model we're going to use MAPE (Mean Absolute Percentage Error) defined as

```math
\frac{1}{n}\sum_{i=1}^n{\frac{|{y_i - \hat y_i}|}{y_i}}
```

as our performance metric. We think this measure fits well on the data since it's interpretation is intuitive.

For our validation strategy we will use 3-fold cross validation on our entire dataset where the performance metric is evaluated on the test set.

As a final step we will update our learner with the optimum set of hyperparameters found in the tuning phase. We'll use that learner going forward with our model fitting and evaluation.


### Tuning results

After a 100 iterations of tuning we have found our best set of parameters:

|Hyperparameter    |  Tuned Result|
|:-----------------|-------------:|
|`nrounds`         |          1606|
|`max_depth`       |             7|
|`eta`             |         0.036|
|`colsample_bytree`|         0.623|
|`subsample`       |         0.796|

|Performance       |              |
|:-----------------|-------------:|
|MAPE              |         0.069|


### R Code
```r
# Tune the model
#tuning Parameters
ps = makeParamSet(
  makeIntegerParam("nrounds", 500, 2500),
  makeIntegerParam("max_depth", 3, 15),
  makeNumericParam("eta", 0.001, 0.2),
  makeNumericParam("colsample_bytree", 0.1, 1),
  makeNumericParam("subsample", 0.5, 1)
  )
#mbo tuning with 100 iterations
tc = makeTuneControlMBO(budget=100)
#tune the paramters based on defined parameters with 3-fold cv
tp = tuneParams(learner_cpo, task, cv3, mape, ps, tc)
#give the learner the optimal hyperparameters
learner_cpo = setHyperPars(learner_cpo, par.vals = tp$x)

#save the variables for later use
save(tp, learner_cpo, df, dateKeys, task, file = "HGG/tuneResults.RData")
```

## Model Feature Effects

Now that we have our learner ready we will use it train a model on the dataset. We're going to train it on a holdout sample with 0.67 split. Then we'll use that model to predict on the test set and evaluate our features. For that purpose we're going to leverage the R `iml` package (Interpretable Machine Learning) to get a sense of how the features are correlated with the price.

### Train the model

We will train our model using our tuned learner on a holdout sample:

```r
# Libraries, datasets and settings
library(dplyr)
library(mlr)
library(mlrCPO)
library(iml)
library(ggplot2)
theme_set(theme_minimal())
load("HGG/tuneResults.RData")

# Feature Effects
#create train test set
set.seed(12345)
ho = makeResampleInstance("Holdout",task)
task.train = subsetTask(task,ho$train.inds[[1]])
task.test = subsetTask(task,ho$test.inds[[1]])
#train the model
mod = train(learner_cpo, task.train)
```

### Create a Predictor object

Next we will use our trained model to create a `Predictor` object from the `iml` package what we'll for our feature effects analysis.

```r
#get the test datasets
X = getTaskData(task.test) %>% select(getTaskFeatureNames(task.test))
y = getTaskData(task.test) %>% select(getTaskTargetNames(task.test))
#create Predictor object used in analysis
predictor = Predictor$new(mod, data=X, y=y)
```

### Feature Importance plots

Feature importance plots are useful in determining the relative importance of a certain feature. What it does is check how the model performance changes if we remove a certain feature. We'll go with our much loved MAPE measure for this purpose.

```r
#get feature importance
imp = FeatureImp$new(predictor, loss="mape")
imp$plot()
```
![Feature importance plot](img/real_estate_report_featimp.png)
