---
### Modelling Real Estate Prices in Reykjavík Area with XGBoost
#### Grettir Heimisson
---

#### Abstract
In this report we're going to create a XGBoost model in R on features provided by Registry Iceland to predict the price of a property. Our objective is to try to maximize predictive power. In doing that we'll do some data cleaning and feature engineering. For the data cleaning part we'll mostly rely on the R `tidyverse` package system. For the feature engineering we'll use our domain knowledge of the data to create new features and also put an upper bound on some of the features to limit the state space. We'll also try some automatic feature engineering in the modelling part. For the modelling part we'll rely on the R `mlr` (machine learning in R) package which is a framework for doing machine learning. We're going to use a tuned XGBoost model for our training, the main reason being it's performance and speed. It has it's drawbacks though, one being it doesn't extrapolate well. So we can't really use it make future predictions. To wrap it all up we'll do model performance checks and some visualization of predictions.

#### Data Cleaning
In this part we're going to get the dataset from Registry Iceland and clean it up. We're going to remove redundant data and features and make sure they're all of the correct data type.

###### R Code
```r
# load the libraries
library(dplyr)
library(lubridate)
library(magrittr)
library(summarytools)

#########################################################################
#-----------------------------DATA CLEANING-----------------------------#
#########################################################################

# the path to the dataset
url_path = paste0(
  "https://www.skra.is/library/Samnyttar-skrar-/Fyrirtaeki-stofnanir/",
  "Fasteignamat-2019/gagnasafn_ib_2018.csv")
# fetch the dataset
df_raw = read.csv2(url_path)
# filter on vars used and capital area and remove small teg_eign categories
# and stig10 < 10 and one outlier m2_price
df = df_raw %>%
  group_by(teg_eign) %>% filter(n()>100) %>% ungroup() %>%
  filter(matssvaedi<999,svfn<2000,stig10==10)
# select the variables we're using and rename them so they're more
# descriptive
df %<>% select(
    id=rfastnum,price_adj=nuvirdi,purchase_date=kdagur,
    area=matssvaedi,subarea=undirmatssvaedi,property_type=teg_eign,
    appartment_type=ibteg,construction_year=byggar,property_m2=birtm2,
    appartment_m2=ibm2,netto_m2=ntm2,area_under_roof_m2=rism2,
    garage_m2=bilskurm2,parking_house_m2=bilgm2,balcony_m2=svalm2,
    main_floor_m2=ib1m2,basement_m2=ib2m2,under_roof_m2=ib3m2,
    floors_no=fjhaed,rooms_no=fjherb,livingrooms_no=fjstof,toilets_no=fjklos,
    kitchens_no=fjeld,storages_no=fjgeym,bathtubs_no=fjbkar,showers_no=fjsturt,
    floor_nr=haednr,top_floor_nr=efstah,appartments_in_building_no=fjibmhl,
    elevators_no=lyfta,circumference=ummal,rating_index=studull,
    appartments_on_floor_no=fjibhaed)
# set kdagur to date
df %<>% mutate(purchase_date = dmy(purchase_date))
# set byggar to date going with january 1.
df %<>% mutate(construction_year = dmy(paste0("0101",construction_year)))
# set some m2 vars to numeric.
m2Cols = c("property_m2", "area_under_roof_m2","area_under_roof_m2",
    "garage_m2", "balcony_m2")
df %<>% mutate_at(m2Cols, ~as.numeric(as.character(gsub(",",".",.))))
# set fastnum to character
df %<>% mutate(id=as.character(id))
# cast negative values to NA
df %<>% mutate_if(is.numeric,~ifelse(.<0,NA,.))
# recode area
value_zone = tribble(
  ~code, ~zone,
  "11" , "Vesturbær: Vestan Bræðraborgarstígs",
  "20" , "Miðbær: Frá Tjörn að Snorrabraut",
  "25" , "Miðbær: Frá Bræðraborgarstíg að Tjörn",
  "31" , "Miðbær: Suður-Þingholt",
  "70" , "Melar: Að sjó",
  "72" , "Hagar/Melar: Vestan Hofsvallagötu",
  "75" , "Skerjafjörður",
  "80" , "Hlíðar",
  "85" , "Kringlan",
  "90" , "Holt/Tún",
  "91" , "Háaleiti/Skeifa",
  "100", "Laugarneshverfi/Vogar",
  "110", "Bryggjuhverfi",
  "120", "Grafarvogur: Hamrar, Foldir, Hús",
  "130", "Grafarvogur: Rimar, Engi, Víkur, Borgir",
  "140", "Grafarvogur: Staðir",
  "150", "Seljahverfi",
  "160", "Hólar, Berg",
  "161", "Fell",
  "170", "Neðra-Breiðholt",
  "180", "Grafarholt",
  "181", "Úlfarsárdalur",
  "200", "Árbær",
  "210", "Ártúnsholt/Höfðar",
  "220", "Selás",
  "270", "Norðlingaholt",
  "280", "Fossvogur",
  "281", "Réttarholt",
  "282", "Blesugróf",
  "283", "Hvassaleiti",
  "284", "Bústaðahverfi",
  "290", "Kjalarnes",
  "300", "Kópavogur: Vesturbær",
  "320", "Kópavogur: Austurbær",
  "330", "Kópavogur: Hjallar, Smárar",
  "340", "Kópavogur: Lindir, Salir",
  "350", "Kópavogur: Hvörf, Þing",
  "351", "Kópavogur: Kórar",
  "400", "Seltjarnarnes",
  "500", "Garðabær",
  "510", "Garðabær: Sjáland",
  "511", "Garðabær: Akrahverfi",
  "520", "Garðabær: Ásar",
  "530", "Garðabær: Vestan Hraunholtsbrautar",
  "540", "Garðabær: Arnarnes",
  "550", "Garðabær: Urriðaholt",
  "560", "Garðabær: Flatir",
  "590", "Garðabær: Utan þéttbýlis",
  "600", "Hafnarfjörður",
  "620", "Hafnarfjörður: Hvaleyrarholt",
  "630", "Hafnarfjörður: Vellir",
  "640", "Hafnarfjörður: Ásland",
  "650", "Hafnarfjörður: Setberg",
  "660", "Hafnarfjörður: Flensborg",
  "670", "Hafnarfjörður: Sunnan byggðar",
  "680", "Hafnarfjörður: Börð",
  "700", "Garðabær: Álftanes",
  "800", "Mosfellsbær",
  "810", "Mosfellsbær: Teigar, Krikar",
  "820", "Mosfellsbær: Leirvogstunga",
  "830", "Mosfellsbær: Mosfellsdalur",
  "840", "Mosfellsbær: Höfðar, Hlíðar",
  "850", "Mosfellsbær: Helgafell",
  "890", "Mosfellsbær: Utan þéttbýlis")
df %<>% mutate(area =
  factor(area,levels=value_zone$code,labels=value_zone$zone))
# set subarea as factor
df %<>% mutate(subarea = factor(subarea,levels=sort(unique(subarea))))
# recode appartment_type
df %<>% mutate_at("appartment_type", ~ifelse(.==11,"sérbýli","fjölbýli"))
df %<>% mutate(appartment_type = as.factor(appartment_type))
# set the price_adj to correct format
df %<>% mutate(price_adj = price_adj*1000)
# drop empty factor levels
df %<>% droplevels()
# drop variables with missing values
df %<>% filter(complete.cases(.))

# Create summary report
options(scipen=1000000)
descr(df, stats=c("mean", "sd", "min", "q1", "med", "q3", "max"),
  transpose=TRUE, style="rmarkdown") %>% knitr::kable(digits=1)
```

Let's have glimpse into how the data looks after that exercise.

|                           |       Mean|    Std.Dev|       Min|         Q1|     Median|         Q3|         Max|
|:--------------------------|----------:|----------:|---------:|----------:|----------:|----------:|-----------:|
|`appartment_m2`              |      104.8|       45.8|      17.7|       74.6|       95.2|      121.3|       420.1|
|`appartments_in_building_no` |       12.7|       15.4|       1.0|        3.0|        7.0|       16.0|       133.0|
|`appartments_on_floor_no`    |        3.2|        3.0|       1.0|        1.0|        2.0|        4.0|        26.0|
|`area_under_roof_m2`         |        0.4|        3.1|       0.0|        0.0|        0.0|        0.0|        89.5|
|`balcony_m2`                 |        6.7|        9.6|       0.0|        0.0|        5.4|        9.4|       269.8|
|`basement_m2`                |        8.1|       24.3|       0.0|        0.0|        0.0|        0.0|       219.0|
|`bathtubs_no`                |        0.8|        0.4|       0.0|        1.0|        1.0|        1.0|         3.0|
|`circumference`              |       90.5|       77.4|       6.1|       47.1|       64.0|      104.0|       689.9|
|`elevators_no`               |        0.4|        0.9|       0.0|        0.0|        0.0|        0.0|         8.0|
|`floor_nr`                   |        2.0|        1.5|       0.0|        1.0|        2.0|        3.0|        16.0|
|`floors_no`                  |        1.2|        0.5|       1.0|        1.0|        1.0|        1.0|         4.0|
|`garage_m2`                  |        7.7|       14.4|       0.0|        0.0|        0.0|        0.0|       115.4|
|`kitchens_no`                |        1.0|        0.2|       0.0|        1.0|        1.0|        1.0|         4.0|
|`livingrooms_no`             |        1.2|        0.5|       0.0|        1.0|        1.0|        1.0|        14.0|
|`main_floor_m2`              |       93.1|       45.2|       0.0|       69.9|       91.4|      115.9|       420.1|
|`netto_m2`                   |       90.2|       39.1|      14.9|       64.1|       82.2|      105.2|       438.6|
|`parking_house_m2`           |        0.5|        3.8|       0.0|        0.0|        0.0|        0.0|        48.2|
|`price_adj`                  | 36683462.4| 17817482.1| 5900000.0| 25128000.0| 32972500.0| 43696000.0| 956146000.0|
|`property_m2`                |      117.8|       56.6|      17.7|       79.9|      103.4|      138.3|       566.0|
|`rating_index`               |        1.0|        0.0|       0.5|        1.0|        1.0|        1.0|         1.1|
|`rooms_no`                   |        2.5|        1.3|       0.0|        2.0|        2.0|        3.0|        24.0|
|`showers_no`                 |        0.6|        0.6|       0.0|        0.0|        1.0|        1.0|         4.0|
|`storages_no`                |        0.6|        0.7|       0.0|        0.0|        0.0|        1.0|         8.0|
|`toilets_no`                 |        1.2|        0.5|       0.0|        1.0|        1.0|        1.0|         5.0|
|`top_floor_nr`               |        3.4|        1.9|       1.0|        2.0|        3.0|        4.0|        13.0|
|`under_roof_m2`              |        3.6|       14.9|       0.0|        0.0|        0.0|        0.0|       176.6|

|`area`                                    |  Freq| % Valid| % Valid Cum.| % Total| % Total Cum.|
|:---------------------------------------|-----:|-------:|------------:|-------:|------------:|
|Vesturbær: Vestan Bræðraborgarstígs     |   341|     1.0|          1.0|     1.0|          1.0|
|Miðbær: Frá Tjörn að Snorrabraut        |  1605|     4.7|          5.7|     4.7|          5.7|
|Miðbær: Frá Bræðraborgarstíg að Tjörn   |   522|     1.5|          7.2|     1.5|          7.2|
|Miðbær: Suður-Þingholt                  |    89|     0.3|          7.4|     0.3|          7.4|
|Melar: Að sjó                           |   493|     1.4|          8.9|     1.4|          8.9|
|Hagar/Melar: Vestan Hofsvallagötu       |  1244|     3.6|         12.5|     3.6|         12.5|
|Skerjafjörður                           |   107|     0.3|         12.8|     0.3|         12.8|
|Hlíðar                                  |   854|     2.5|         15.3|     2.5|         15.3|
|Kringlan                                |   246|     0.7|         16.0|     0.7|         16.0|
|Holt/Tún                                |  1686|     4.9|         20.9|     4.9|         20.9|
|Háaleiti/Skeifa                         |   517|     1.5|         22.4|     1.5|         22.4|
|Laugarneshverfi/Vogar                   |  2305|     6.7|         29.1|     6.7|         29.1|
|Bryggjuhverfi                           |   179|     0.5|         29.6|     0.5|         29.6|
|Grafarvogur: Hamrar, Foldir, Hús        |   739|     2.2|         31.8|     2.2|         31.8|
|Grafarvogur: Rimar, Engi, Víkur, Borgir |  1433|     4.2|         36.0|     4.2|         36.0|
|Grafarvogur: Staðir                     |   109|     0.3|         36.3|     0.3|         36.3|
|Seljahverfi                             |   967|     2.8|         39.1|     2.8|         39.1|
|Hólar, Berg                             |   977|     2.8|         41.9|     2.8|         41.9|
|Fell                                    |   555|     1.6|         43.6|     1.6|         43.6|
|Neðra-Breiðholt                         |   630|     1.8|         45.4|     1.8|         45.4|
|Grafarholt                              |   658|     1.9|         47.3|     1.9|         47.3|
|Úlfarsárdalur                           |   204|     0.6|         47.9|     0.6|         47.9|
|Árbær                                   |   609|     1.8|         49.7|     1.8|         49.7|
|Ártúnsholt/Höfðar                       |   149|     0.4|         50.1|     0.4|         50.1|
|Selás                                   |   482|     1.4|         51.5|     1.4|         51.5|
|Norðlingaholt                           |   436|     1.3|         52.8|     1.3|         52.8|
|Fossvogur                               |   522|     1.5|         54.3|     1.5|         54.3|
|Réttarholt                              |   346|     1.0|         55.3|     1.0|         55.3|
|Blesugróf                               |    13|     0.0|         55.3|     0.0|         55.3|
|Hvassaleiti                             |   285|     0.8|         56.2|     0.8|         56.2|
|Bústaðahverfi                           |   232|     0.7|         56.8|     0.7|         56.8|
|Kjalarnes                               |    45|     0.1|         57.0|     0.1|         57.0|
|Kópavogur: Vesturbær                    |   841|     2.4|         59.4|     2.4|         59.4|
|Kópavogur: Austurbær                    |  1820|     5.3|         64.7|     5.3|         64.7|
|Kópavogur: Hjallar, Smárar              |   801|     2.3|         67.0|     2.3|         67.0|
|Kópavogur: Lindir, Salir                |  1015|     3.0|         70.0|     3.0|         70.0|
|Kópavogur: Hvörf, Þing                  |   610|     1.8|         71.8|     1.8|         71.8|
|Kópavogur: Kórar                        |   891|     2.6|         74.4|     2.6|         74.4|
|Seltjarnarnes                           |   633|     1.8|         76.2|     1.8|         76.2|
|Garðabær                                |   898|     2.6|         78.8|     2.6|         78.8|
|Garðabær: Sjáland                       |   434|     1.3|         80.1|     1.3|         80.1|
|Garðabær: Akrahverfi                    |   207|     0.6|         80.7|     0.6|         80.7|
|Garðabær: Ásar                          |   175|     0.5|         81.2|     0.5|         81.2|
|Garðabær: Vestan Hraunholtsbrautar      |    45|     0.1|         81.3|     0.1|         81.3|
|Garðabær: Arnarnes                      |    58|     0.2|         81.5|     0.2|         81.5|
|Garðabær: Urriðaholt                    |   200|     0.6|         82.1|     0.6|         82.1|
|Garðabær: Flatir                        |    57|     0.2|         82.2|     0.2|         82.2|
|Hafnarfjörður                           |  2174|     6.3|         88.6|     6.3|         88.6|
|Hafnarfjörður: Hvaleyrarholt            |   242|     0.7|         89.3|     0.7|         89.3|
|Hafnarfjörður: Vellir                   |   896|     2.6|         91.9|     2.6|         91.9|
|Hafnarfjörður: Ásland                   |   445|     1.3|         93.2|     1.3|         93.2|
|Hafnarfjörður: Setberg                  |   368|     1.1|         94.3|     1.1|         94.3|
|Hafnarfjörður: Flensborg                |    70|     0.2|         94.5|     0.2|         94.5|
|Hafnarfjörður: Börð                     |   465|     1.4|         95.8|     1.4|         95.8|
|Garðabær: Álftanes                      |   296|     0.9|         96.7|     0.9|         96.7|
|Mosfellsbær                             |   269|     0.8|         97.5|     0.8|         97.5|
|Mosfellsbær: Teigar, Krikar             |   283|     0.8|         98.3|     0.8|         98.3|
|Mosfellsbær: Leirvogstunga              |    41|     0.1|         98.4|     0.1|         98.4|
|Mosfellsbær: Höfðar, Hlíðar             |   376|     1.1|         99.5|     1.1|         99.5|
|Mosfellsbær: Helgafell                  |   175|     0.5|        100.0|     0.5|        100.0|
|<NA>                                    |     0|      NA|           NA|     0.0|        100.0|
|Total                                   | 34364|   100.0|        100.0|   100.0|        100.0|

|`subarea`|  Freq| % Valid| % Valid Cum.| % Total| % Total Cum.|
|:-----|-----:|-------:|------------:|-------:|------------:|
|0     | 28982|    84.3|         84.3|    84.3|         84.3|
|1     |   283|     0.8|         85.2|     0.8|         85.2|
|2     |    66|     0.2|         85.4|     0.2|         85.4|
|3     |   197|     0.6|         85.9|     0.6|         85.9|
|4     |    65|     0.2|         86.1|     0.2|         86.1|
|6     |    31|     0.1|         86.2|     0.1|         86.2|
|7     |    19|     0.1|         86.3|     0.1|         86.3|
|8     |    18|     0.1|         86.3|     0.1|         86.3|
|9     |     6|     0.0|         86.3|     0.0|         86.3|
|10    |     6|     0.0|         86.3|     0.0|         86.3|
|11    |     5|     0.0|         86.4|     0.0|         86.4|
|12    |   214|     0.6|         87.0|     0.6|         87.0|
|13    |    91|     0.3|         87.3|     0.3|         87.3|
|14    |    47|     0.1|         87.4|     0.1|         87.4|
|15    |   126|     0.4|         87.8|     0.4|         87.8|
|16    |    64|     0.2|         87.9|     0.2|         87.9|
|17    |   232|     0.7|         88.6|     0.7|         88.6|
|18    |   331|     1.0|         89.6|     1.0|         89.6|
|20    |   116|     0.3|         89.9|     0.3|         89.9|
|21    |   189|     0.5|         90.5|     0.5|         90.5|
|22    |   348|     1.0|         91.5|     1.0|         91.5|
|23    |    72|     0.2|         91.7|     0.2|         91.7|
|24    |   146|     0.4|         92.1|     0.4|         92.1|
|25    |   125|     0.4|         92.5|     0.4|         92.5|
|26    |    75|     0.2|         92.7|     0.2|         92.7|
|27    |    30|     0.1|         92.8|     0.1|         92.8|
|28    |   206|     0.6|         93.4|     0.6|         93.4|
|31    |    21|     0.1|         93.4|     0.1|         93.4|
|32    |     7|     0.0|         93.5|     0.0|         93.5|
|33    |     9|     0.0|         93.5|     0.0|         93.5|
|34    |    10|     0.0|         93.5|     0.0|         93.5|
|35    |    10|     0.0|         93.5|     0.0|         93.5|
|36    |    10|     0.0|         93.6|     0.0|         93.6|
|37    |    34|     0.1|         93.7|     0.1|         93.7|
|38    |    10|     0.0|         93.7|     0.0|         93.7|
|39    |     4|     0.0|         93.7|     0.0|         93.7|
|40    |    33|     0.1|         93.8|     0.1|         93.8|
|41    |     2|     0.0|         93.8|     0.0|         93.8|
|42    |     5|     0.0|         93.8|     0.0|         93.8|
|43    |     8|     0.0|         93.9|     0.0|         93.9|
|44    |   245|     0.7|         94.6|     0.7|         94.6|
|45    |    59|     0.2|         94.7|     0.2|         94.7|
|46    |   213|     0.6|         95.4|     0.6|         95.4|
|47    |    73|     0.2|         95.6|     0.2|         95.6|
|48    |    54|     0.2|         95.7|     0.2|         95.7|
|49    |   157|     0.5|         96.2|     0.5|         96.2|
|50    |    15|     0.0|         96.2|     0.0|         96.2|
|51    |    78|     0.2|         96.5|     0.2|         96.5|
|52    |   101|     0.3|         96.8|     0.3|         96.8|
|53    |     2|     0.0|         96.8|     0.0|         96.8|
|54    |    17|     0.0|         96.8|     0.0|         96.8|
|55    |    17|     0.0|         96.9|     0.0|         96.9|
|56    |    34|     0.1|         97.0|     0.1|         97.0|
|57    |    14|     0.0|         97.0|     0.0|         97.0|
|59    |   101|     0.3|         97.3|     0.3|         97.3|
|60    |    78|     0.2|         97.5|     0.2|         97.5|
|61    |    19|     0.1|         97.6|     0.1|         97.6|
|62    |   198|     0.6|         98.1|     0.6|         98.1|
|63    |    29|     0.1|         98.2|     0.1|         98.2|
|64    |    10|     0.0|         98.3|     0.0|         98.3|
|65    |    16|     0.0|         98.3|     0.0|         98.3|
|101   |    25|     0.1|         98.4|     0.1|         98.4|
|102   |    11|     0.0|         98.4|     0.0|         98.4|
|103   |    15|     0.0|         98.5|     0.0|         98.5|
|104   |   189|     0.5|         99.0|     0.5|         99.0|
|105   |   190|     0.6|         99.6|     0.6|         99.6|
|106   |    22|     0.1|         99.6|     0.1|         99.6|
|107   |   129|     0.4|        100.0|     0.4|        100.0|
|<NA>  |     0|      NA|           NA|     0.0|        100.0|
|Total | 34364|   100.0|        100.0|   100.0|        100.0|

|`property_type`|  Freq| % Valid| % Valid Cum.| % Total| % Total Cum.|
|:-----------|-----:|-------:|------------:|-------:|------------:|
|Einbýlishús |  3057|     8.9|          8.9|     8.9|          8.9|
|Íbúðareign  | 28391|    82.6|         91.5|    82.6|         91.5|
|Parhús      |   749|     2.2|         93.7|     2.2|         93.7|
|Raðhús      |  2167|     6.3|        100.0|     6.3|        100.0|
|<NA>        |     0|      NA|           NA|     0.0|        100.0|
|Total       | 34364|   100.0|        100.0|   100.0|        100.0|

|`appartment_type` |  Freq| % Valid| % Valid Cum.| % Total| % Total Cum.|
|:--------|-----:|-------:|------------:|-------:|------------:|
|fjölbýli | 26127|      76|           76|      76|           76|
|sérbýli  |  8237|      24|          100|      24|          100|
|<NA>     |     0|      NA|           NA|       0|          100|
|Total    | 34364|     100|          100|     100|          100|

With this summary in mind we can start our feature engineering.

#### Feature Engineering

Now we're going to engineer the data so it will better fit into our XGBoost model.

We're going to start by capping the possible values for the numerical variables. The reason is that we don't want the learner to make inference about events that are highly unlikely. We'll make a simple rule. We'll cap the bins at the 0.0001 and 0.9999 quantiles. That is only 0.02% cap per feature. We'll make a note on how many lines are changed with this method.

```r
#########################################################################
#--------------------------FEATURE ENGINEERING--------------------------#
#########################################################################

# cap the numerics to a 0.0001 and 0.9999 quantile range
df_old = df
df %<>% mutate_if(is.numeric,
  ~pmax(pmin(.,quantile(.,0.9999)),quantile(.,0.0001)))
df %>% anti_join(df_old) %>% tally() #N=64
rm(df_old)
```

With this we only affected 64 measurements and got rid of the most extreme values in the dataset.

We're also going to make all the square meter variables as a fraction of the total property area. This is nice since get all the variables on the same scale and we think that those variable only matter in relation to the total area of the property.

```r
# set all m2 as percentage of property_m2
m2Cols = grep("m2", colnames(df), value=TRUE) %>% setdiff("property_m2")
df %<>% mutate_at(m2Cols, ~./property_m2)
```

Next we will create a total room number variable. So we'll count every room equally and get rid of all the others. This we reduce our feature space which makes it easier for training.

```r
# Add together all rooms for a single total_rooms_no variable
df %<>% mutate(total_rooms_no =
    rooms_no+livingrooms_no+toilets_no+kitchens_no+storages_no) %>%
  select(-c("livingrooms_no","toilets_no","kitchens_no","storages_no"))
```

Create a new variable telling us the price of last selling price of the property along with date.

```r
# Create a new variable telling us the price and date of last sale
df %>% group_by(id) %<>%
  mutate(last_sold_price = lag(price_adj, 1, order_by=purchase_date),
    last_sold_date = lag(purchase_date, 1, order_by=purchase_date))
```

Some small feature engineering
```r
# Add bathtubs and showers and let shower be an indicator variable
df %<>% mutate(
  showers_bathtubs_no = showers_no+bathtubs_no,
  has_shower = 1*(showers_no>0)) %>% select(-showers_no, -bathtubs_no)
# Make elevator an indicator variable
df %<>% mutate(has_elevator = 1*(elevators_no>0)) %>% select(-elevators_no)
# Make a new variable that tells us the age of the property when it was bought
df %<>% mutate(
  property_age_when_sold = year(purchase_date)-year(construction_year))

# Save the dataset for later usage
save(df, file="HGG/df.RData")
```

That's that. We'll handle the factor date features engineering in the model building stage.

#### Modelling with XGBoost

Now we get to the fun part. Our features are nice and clean after the previous steps and we can begin to train our model.

##### Preprocessing dates and factors

We will be changing all our date variables to numeric since our modelling framework can't handle data data. We're going to keep all the dates stored in a key dataframe so we can get the original dates back when evaulating the model.

We will preprocess our factors in the same way, casting them all to numeric. We can do that since there is some natural ordering of the factor levels, numbers close together for the `area` variable tend to be geographically close. We're not sure about the `subarea` variable but we're allowing it since it simplifies our learning process. The casting of factors to numbers will be done in the learning stage so our dataset will keep the initial factor levels.

##### Defining The Learner and Training Task

We're going to use a XGBoost regression tree model. Our objective will use a Gamma link function since our target variable price has heavy tails and is skewed. We're going for a histogram tree method since it's faster.

For our training task we will use the data preprocessed in previous sections with `price_adj` (price adjusted for inflation) as the target variable. We will exclude the `id` variable from the task but use all others.

We're defing our learner and training task in R mlr framework which allows for some high level operations.

###### R Code
```r
################################################################################
#---------------------------PREPROCESS, LEARNER, TASK--------------------------#
################################################################################

# Load data, libs and apply settings
library(dplyr)
library(mlr)
library(mlrCPO)
library(lubridate)
library(magrittr)
# Load the df
load("HGG/df.RData")
# internal stuff for mlr
configureMlr(on.par.without.desc="warn")

# Preprocessing
#date keys
dateKeys = df %>% select_if(is.Date)
#cast df date to numerics
df = df %<>% mutate_if(is.Date, as.numeric)
#convert all factors to numeric cpo
cpos = cpoAsNumeric()

# Define Learner and Task
#define the learner
learner = makeLearner("regr.xgboost")
#make the learner run with reg:gamma, gbtree and hist.
learner = learner %>% setHyperPars(par.vals=
    list(objective="reg:gamma",booster="gbtree",tree_method ="hist"))
#apply the cpo to the learner
learner_cpo = cpos %>>% learner
# Make a regression task with price_adj as target and remove id
task = makeRegrTask(data=within(df, rm(id)), target="price_adj")
```

##### Model Tuning

We're going to tune the hyperparameters of th XGBoost model so it will find a viable set of hyperparameters to train the learner.

###### Tuning Parameters

The parameters we're going to tune are summed up in the following table:

|Hyperparameter    |Explanation                                           |
|:-----------------|:-----------------------------------------------------|
|`nrounds`         |The number of rounds for boosting                     |
|`max_depth`       |Maximum depth of a tree                               |
|`eta`             |Step size shrinkage used in update                    |
|`colsample_bytree`|subsample ratio of columns when constructing each tree|
|`subsample`       |Subsample ratio of the training instances             |

We suspect that the model will be complex so we're going to allow `nrounds` and `max_depth` to be large and we're using `eta`, `colsample_bytree` and `subsample` to prevent overfitting. The tuning parameters are as follows

|Hyperparameter    |  Lower bound|  Upper Bound|
|:-----------------|------------:|------------:|
|`nrounds`         |          500|         2500|
|`max_depth`       |            3|           15|
|`eta`             |        0.001|          0.2|
|`colsample_bytree`|          0.1|            1|
|`subsample`       |          0.5|            1|

###### Hyperparameter Search Strategy

In the search of the best set of hyperparameters we're going for an MBO (Model Based Optimization) strategy. What this strategy does is it fit's a surrogate model over the space of hyper-parameters and searches for promising points that maximize our performance metric. Our mbo uses Kriging to search for those points. We do 100 iterations of this search which will hopefully lead to a set a hyperparameters that perform well in cross validation.

###### Performance Metrics and Validation Strategy

When tuning our model we're going to use MAPE (Mean Absolute Percentage Error) defined as

\[\frac{1}{n}\sum_{i=1}^n{\frac{|{y_i - \hat y_i}|}{y_i}}\]

as our performance metric. We think this measure fits well on the data since it's interpretation is intuitive.

For our validation strategy we will use 3-fold cross validation on our entire dataset where the performance metric is evaluated on the test set.

As a final step we will update our learner with the optimum set of hyperparameters found in the tuning phase. We'll use that learner going forward with our model fitting and evaluation.


###### Tuning results

After a 100 iterations of tuning we have found our best set of parameters:

|Hyperparameter    |  Tuned Result|
|:-----------------|-------------:|
|`nrounds`         |          1606|
|`max_depth`       |             7|
|`eta`             |         0.036|
|`colsample_bytree`|         0.623|
|`subsample`       |         0.796|

|Performance       |              |
|:-----------------|-------------:|
|MAPE              |         0.069|


###### R Code
```r
# Tune the model
#tuning Parameters
ps = makeParamSet(
  makeIntegerParam("nrounds", 500, 2500),
  makeIntegerParam("max_depth", 3, 15),
  makeNumericParam("eta", 0.001, 0.2),
  makeNumericParam("colsample_bytree", 0.1, 1),
  makeNumericParam("subsample", 0.5, 1)
  )
#mbo tuning with 100 iterations
tc = makeTuneControlMBO(budget=100)
#tune the paramters based on defined parameters with 3-fold cv
tp = tuneParams(learner_cpo, task, cv3, mape, ps, tc)
#give the learner the optimal hyperparameters
learner_cpo = setHyperPars(learner_cpo, par.vals = tp$x)

#save the variables for later use
save(tp, learner_cpo, df, dateKeys, task, file = "HGG/tuneResults.RData")
```

##### Model Feature Effects

Now that we have our learner ready we will use it train a model on the dataset. We're going to train it on a holdout sample with 0.67 split. Then we'll use that model to predict on the test set and evaluate our features. For that purpose we're going to leverage the R `iml` package (Interpretable Machine Learning) to get a sense of how the features are correlated with the price.

###### Train the model

We will train our model using our tuned learner on a holdout sample:

```r
# Libraries, datasets and settings
library(dplyr)
library(mlr)
library(mlrCPO)
library(iml)
library(ggplot2)
theme_set(theme_minimal())
load("HGG/tuneResults.RData")

# Feature Effects
#create train test set
set.seed(12345)
ho = makeResampleInstance("Holdout",task)
task.train = subsetTask(task,ho$train.inds[[1]])
task.test = subsetTask(task,ho$test.inds[[1]])
#train the model
mod = train(learner_cpo, task.train)
```

###### Create a Predictor object

Next we will use our trained model to create a `Predictor` object from the `iml` package what we'll for our feature effects analysis.

```r
#get the test datasets
X = getTaskData(task.test) %>% select(getTaskFeatureNames(task.test))
y = getTaskData(task.test) %>% select(getTaskTargetNames(task.test))
#create Predictor object used in analysis
predictor = Predictor$new(mod, data=X, y=y)
```

###### Feature Importance plots

Feature importance plots are useful in determining the relative importance of a certain feature. What it does is check how the model performance changes if we remove a certain feature. We'll go with our much loved MAPE measure for this purpose.

```r
#get feature importance
imp = FeatureImp$new(predictor, loss="mape")
imp$plot()
```

![Alt text](./impplot.svg =750x)
<img src="./impplot.svg">
