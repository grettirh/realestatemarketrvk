library(dplyr)
library(data.table)
library(ggplot2)
library(mlr)
theme_set(theme_minimal())

# comparisons of prediction mean and variance distribution vs. sample distribution

load("HGG/data/bootstrap.RData")
load("HGG/data/dt.RData")

# Let's try a single appartment with the most sales and fit a time series from
# the bootstrapped models and put the actual sales with it
setkey(dt, rfastnum)

dt_task = dt %>%
  as.data.frame() %>%
  createDummyFeatures() %>%
  as.data.table()

dt_task[, c("kdagur", "m2_price") := NULL]
dt_task = dt_task[, .SD[.N,], by=rfastnum]

# get the year 2017
months = 1:max(dt$kdagur)
pred_data = merge(CJ(rfastnum=dt_task[["rfastnum"]],
  kdagur=months), dt_task, by="rfastnum")

setkey(pred_data, kdagur, rfastnum)

predCols = getLearnerModel(res$models[[1]])$feature_names
rfast = unique(pred_data$rfastnum)

models = res$models

rm(dt, dt_task, res)

for (month in months){
   data = as.data.frame(pred_data[kdagur==month,.SD, .SDcols = predCols])
   pred = lapply(models,
    function(x) predict(x, newdata=data)$data$response)
   predict_data = cbind.data.frame(rfastnum=rfast, month=month,
      pred=unlist(pred))
    save(predict_data,
      file=paste0("HGG/data/ts_predict_data_m", month, ".RData"))
    rm(predict_data)
    gc()
}

library(data.table)

# comparisons of prediction mean and variance distribution vs. sample distribution

#load("HGG/data/dt.RData")
load("HGG/data/ts_predict_data_m1.RData")

dt_ds = as.data.table(copy(predict_data))
rm(predict_data); gc(); keys = dt_ds$rfastnum;

set(dt_ds, j="rfastnum", value = seq_along(dt_ds[["rfastnum"]]))
set(dt_ds, j="pred", value=round(dt_ds[["pred"]], 1))
setnames(dt_ds, c("month", "pred"), c("m1", "pred1")); gc();
setkey(dt_ds, rfastnum)

for(m in 2:74){
  gc();
  load(paste0("HGG/data/ts_predict_data_m", m, ".RData")); gc();
  dt_ds_temp = as.data.table(predict_data); rm(predict_data); gc();

  set(dt_ds_temp, j="rfastnum", value=seq_along(dt_ds_temp[["rfastnum"]]))
  gc()
  set(dt_ds_temp, j="pred", value=round(dt_ds_temp[["pred"]], 1)); gc();
  setnames(dt_ds_temp, c("month", "pred"), paste0(c("m", "pred"), m)); gc();
  setkey(dt_ds_temp, rfastnum)
  dt_ds = merge(dt_ds, dt_ds_temp); gc();
  rm(dt_ds_temp); gc();
}
rm(m)

save(dt_ds, file="HGG/data/dt_ds.RData")
